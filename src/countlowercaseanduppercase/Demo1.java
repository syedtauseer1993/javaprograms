package countlowercaseanduppercase;

import java.util.Scanner;

public class Demo1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Combination of Lower and Upper Case String : ");
		String str = sc.next();
		int lower = 0;
		int upper = 0;
		for (int i = 0; i < str.length(); i++) {
			char ch = str.charAt(i);
			if (ch >= 'A' && ch <= 'Z') {
				upper++;
			} else if (ch >= 'a' && ch <= 'z') {
				lower++;
			}
		}
		System.out.println("UpperCase Letters : " + lower);
		System.out.println("LowerCase Letters : " + upper);
		sc.close();
	}

}
