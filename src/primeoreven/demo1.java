package primeoreven;

import java.util.Scanner;

public class demo1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the max value");
		int n = sc.nextInt();
		System.out.println("Even number are :" + n);
		for (int i = 0; i <= n; i++) {
			if (i % 2 == 0) {
				System.out.print(i + " ");
			}
		}
		System.out.println("\nOdd number from 0 to : " + n);
		for (int j = 0; j <= n; j++) {
			if (j % 2 == 1) {
				System.out.print(j + " ");
			}
		}
		sc.close();
	}
}
