package primeoreven;

import java.util.Scanner;

public class Demo2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int n = sc.nextInt();
		if (n % 2 == 0) {
			System.out.println("Its Even Number : " + n);
		} else {
			System.out.println("Its Odd Number : " + n);
		}
		sc.close();
	}
}
