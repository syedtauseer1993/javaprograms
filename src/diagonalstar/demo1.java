package diagonalstar;

import java.util.Scanner;

public class demo1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int n = sc.nextInt();
		int count = 1;
		System.out.println("***********************************************************************************");
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(i + " ");
			}
			System.out.println(" ");
		}
		System.out.println("***********************************************************************************");
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(j + " ");
			}
			System.out.println(" ");
		}
		System.out.println("***********************************************************************************");
		for (int i = 1; i <= n; i++) {
			for (int j = 1; j <= i; j++) {
				System.out.print(count + " ");
				count++;
			}
			System.out.println();
		}
		System.out.println("***********************************************************************************");
		for (int i = 1; i <= n; i++) {
			for (int j = n; j >= i; j--) {
				System.out.print(i + " ");
			}
			System.out.println(" ");
		}
		System.out.println("***********************************************************************************");
		for (int i = 1; i <= n; i++) {
			for (int j = n; j >= i; j--) {
				System.out.print(j + " ");
			}
			System.out.println(" ");
		}
		System.out.println("***********************************************************************************");
		for (int i = 1; i <= n; i++) {
			for (int j = n; j >= i; j--) {
				System.out.print(count + " ");
				count++;
			}
			System.out.println(" ");
		}
		System.out.println("***********************************************************************************");
		sc.close();
	}
}
