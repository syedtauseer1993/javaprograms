package areaofcircle;

import java.util.Scanner;

public class Demo1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		System.out.println();
		double r = sc.nextDouble();
		double areaofCircle = 3.141 * r * r;
		System.out.println(areaofCircle);
		sc.close();
	}
}
