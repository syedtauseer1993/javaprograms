package primenumber;

import java.util.Scanner;

public class Demo1 {
	public static void main(String[] args) {
		int i = 2;
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int n = sc.nextInt();
		boolean flag = false;
		while (i <= n / 2) {
			if (n % i == 0) {
				flag = true;
				break;
			}
			i++;

		}
		if (!flag) {
			System.out.println("It's a prime number");
		} else {
			System.out.println("It's not a prime number");
		}
		sc.close();
	}
}
