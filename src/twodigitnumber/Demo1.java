package twodigitnumber;

import java.util.Scanner;

public class Demo1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int n = sc.nextInt();
		if (n >= 10 && n <= 99) {
			System.out.println("Its is a two digit number : " + n);
		} else {
			System.out.println("Its is not a two digit number : " + n);
		}
		sc.close();
	}
}
