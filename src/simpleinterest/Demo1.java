package simpleinterest;

import java.util.Scanner;

public class Demo1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Principle Value : ");
		int p = sc.nextInt();
		System.out.println("Enter Time : ");
		int t = sc.nextInt();
		System.out.println("Enter Rate Of Interest : ");
		int r = sc.nextInt();
		int simpleInterest = p * t * r / 100;
		System.out.println("SimpleInterest is : " + simpleInterest);
		sc.close();
	}
}
