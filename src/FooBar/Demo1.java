package FooBar;

import java.util.Scanner;

public class Demo1 {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Nmber");
		int n = sc.nextInt();

		if (n % 3 == 0 && n % 5 == 0) {
			System.out.println("Its a foobar : " + n);
		} else if (n % 3 == 0) {
			System.out.println("its a foo : " + n);
		} else if (n % 5 == 0) {
			System.out.println("Its a bar : " + n);
		} else {
			System.out.println(n + ":" + "Its is not in(foobar,foo,bar)");
		}

		sc.close();
	}
}
