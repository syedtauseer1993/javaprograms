package palindrome;

import java.util.Scanner;

public class Demo1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int n = sc.nextInt();
		String str = n + "";
		StringBuilder sb = new StringBuilder(str);
		String rev = sb.reverse().toString();
		System.out.println(rev);
		System.out.println(str.equals(rev) ? "Its a Palindrome " : "Its not a Palindrome");
		sc.close();
	}
}
