package palindrome;

import java.util.Scanner;

public class Demo2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Number");
		int n = sc.nextInt();
		int rev = 0;
		int temp = n;
		while (n > 0) {
			int last = n % 10;
			rev = rev * 10 + last;
			n = n / 10;

		}
		if (temp == rev) {
			System.out.println("Its a Palindrome");
		} else {
			System.out.println("Its not a Palindrome");
		}
	}
}
