package power;

import java.util.Scanner;

public class Demo1 {
	static int pow(int m, int n) {
		int res = 1;
		for (int i = 1; i <= n; i++) {
			res = res * m;

		}
		return res;
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("enter the value of m");
		int m = sc.nextInt();
		System.out.println("enter the value of n");
		int n = sc.nextInt();
		System.out.println(pow(m, n));
		sc.close();
	}
}
